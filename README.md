# Code



## Getting started

This code is written and test in VS code

Clone this repo, open it in VS code.
Go to assign.cpp

In the top ribbon of vs code select "Run", Under run option select "Start Debugging".
If further option asked select g++ build and debug active file.
In the bottom window select terminal to see output.

## Assumptions

Workspace is from (-1, -1) -> (1, 1).
Assume this workspace is divided in grid cells with resolution 0.2 (can choose diffrent resolution as well).

So there will be 10 horizontal and vertical grid cell.
With coordinate of leftmost and bottom-most cell coordinate (-0.8, -0.8).
One step right is (-0.6, -0,8) and right to this is (-0.4, -0,8) so on.

One cell up to (-0.8, -0.8) is (-0.8, -0.6) and so on.

Assumption is also made on robots destination. Robot stops at a particular point after exploring the workspace.
Chosen destination is (-1, -1).